function matchesPerYear(matches) {
  let result = matches.reduce((acc, match) => {
    let season = match.season;
    if (!acc.hasOwnProperty(season)) {
      acc[season] = 1;
    } else {
      acc[season] += 1;
    }
    return acc;
  }, {});
  return result;
}

module.exports = matchesPerYear;
