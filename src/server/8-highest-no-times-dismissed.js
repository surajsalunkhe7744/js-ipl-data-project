function bowlerSuperOverDeliveries(deliveries) {
  let wicketTakingDeliveries = deliveries.filter(
    (delivery) =>
      delivery.dismissal_kind != "run out" &&
      delivery.player_dismissed.length != 0
  );
  
  let bowlerWicketDetails = wicketTakingDeliveries.reduce(
    (acc, wicketDelivery) => {
      let bowlerName = wicketDelivery.bowler;
      let batsmanName = wicketDelivery.batsman;
      if (acc.hasOwnProperty(bowlerName)) {
        if (acc[bowlerName].hasOwnProperty(batsmanName)) {
          acc[bowlerName][batsmanName] =
            acc[bowlerName][batsmanName] + 1;
        } else {
          acc[bowlerName][batsmanName] = 1;
        }
      } else {
        acc[bowlerName] = { [batsmanName]: 1 };
      }
      return acc;
    },
    {}
  );

  let bowlerNamesKey = Object.keys(bowlerWicketDetails);
  let bowlerNamesValues = Object.values(bowlerWicketDetails);
  let sortedBallerWIcketDetails = bowlerNamesValues.reduce(
    (acc, bowlerWicket, index) => {
      let economySort = Object.entries(bowlerWicket)
        .sort(([, item1], [, item2]) => item1 - item2)
        .splice(0,1)
        .reduce((rem, [key, val]) => ({ ...rem, [key]: val }), {});
      let economySortKey = Object.keys(economySort);
      let economySortValues = Object.values(economySort);
      acc[bowlerNamesKey[index]] = { [economySortKey[0]]: economySortValues[0] };
      return acc;
    },
    {}
  );
  return sortedBallerWIcketDetails;
}

module.exports = bowlerSuperOverDeliveries;
