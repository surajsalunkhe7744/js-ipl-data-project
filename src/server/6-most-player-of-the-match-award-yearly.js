function playerOfTheMatchData(matches) {
  let manOfTheMatch = matches.reduce((acc, match) => {
    if (acc.hasOwnProperty(match.season)) {
      if (acc[match.season].hasOwnProperty(match.player_of_match)) {
        acc[match.season][match.player_of_match] += 1;
      } else {
        acc[match.season][match.player_of_match] = 1;
      }
    } else {
      acc[match.season] = { [match.player_of_match]: 1 };
    }
    return acc;
  }, {});

  let seasonalManOfTheMatchKey = Object.keys(manOfTheMatch);
  let seasonalManOfTheMatchValues = Object.values(manOfTheMatch);

  let sortedManOfTheMatchDetails = seasonalManOfTheMatchValues.reduce(
    (acc, playerOfTheMatch, index) => {
      let playerOfTheMatchSort = Object.entries(playerOfTheMatch)
        .sort(([, item1], [, item2]) => item2 - item1).splice(0,1)
        .reduce((rem, [key, val]) => ({ ...rem, [key]: val }), {});
      let sortKeyIndex = Object.keys(playerOfTheMatchSort);
      let sortValueIndex = Object.values(playerOfTheMatchSort);
      acc[seasonalManOfTheMatchKey[index]] = { [sortKeyIndex[0]]: sortValueIndex[0] };
      return acc;
    },
    {}
  );
  return sortedManOfTheMatchDetails;
}

module.exports = playerOfTheMatchData;
