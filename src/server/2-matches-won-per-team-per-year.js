function matchesWonPerTeamOnYear(matches) {
  let result = matches.reduce((acc, match) => {
    let season = match.season;
    let winnerTeam = match.winner;
    if (acc.hasOwnProperty(season)) {
      if (acc[season].hasOwnProperty(winnerTeam)) {
        acc[season][winnerTeam] += 1;
      } else {
        acc[season][winnerTeam] = 1;
      }
    } else {
      acc[season] = { [winnerTeam]: 1 };
    }
    return acc;
  }, {});
  return result;
}

module.exports = matchesWonPerTeamOnYear;
