const csvToJson = require("csvtojson");
const path = require("path");
const fs = require("fs");
const matchesPerYear = require("./1-matches-per-year");
const matchesWonPerTeamOnYear = require("./2-matches-won-per-team-per-year");
const extraRunCalculator = require("./3-matches-extra-run-per-team-year-2016");
const bowlerEconomyCalculator = require("./4-top-10-economical-bowlers-in-year-2015");
const countOfMatchAndTossBothWonTeams = require("./5-count-match-and-toss-won");
const playerOfTheMatchData = require("./6-most-player-of-the-match-award-yearly");
const strikeRateCalculator = require("./7-batsman-strike-rate-each-season");
const bowlerSuperOverDeliveries = require("./8-highest-no-times-dismissed");
const bowlerSuperOverDetails = require("./9-superOver-best-economy-bowler");

csvToJson()
  .fromFile(path.join(__dirname,"../data/deliveries.csv"))
  .then((deliveries) => {

    csvToJson()
      .fromFile(path.join(__dirname,"../data/matches.csv"))
      .then((matches) => {

        /* First Program - 1-matches-per-year.js  6ms*/
        // console.time();
        // let result = matchesPerYear(matches);
        // console.log(result);
        // console.timeEnd();

        /* Second Program - 2-matches-won-per-team-per-year.js  8ms*/
        // console.time();
        // let result = matchesWonPerTeamOnYear(matches);
        // console.log(result);
        // console.timeEnd();

        /* Third Program - 3-matches-extra-run-per-team-year-2016.js  500ms*/
        // console.time();
        // let result = extraRunCalculator(deliveries,matches,"2016");
        // console.log(result);
        // console.timeEnd();


         /* Fourth Program - 4-top-10-economical-bowlers-in-year-2015.js  500ms*/
        //  console.time();
        //  let result = bowlerEconomyCalculator(deliveries,matches,"2015");
        //  console.log(result);
        //  console.timeEnd();

        /* Fifth Program - 5-count-match-and-toss-won.js  4ms*/
        // console.time();
        //  let result = countOfMatchAndTossBothWonTeams(matches);
        //  console.log(result);
        //  console.timeEnd();

        /* Sixth Program - 6-most-player-of-the-match-award-yearly.js  8ms */
        // console.time();
        //  let result = playerOfTheMatchData(matches);
        //  console.log(result);
        //  console.timeEnd();

        
        /* Seventh Program - 7-batsman-strike-rate-each-season.js  700ms*/
        // console.time();
        //  let result = strikeRateCalculator(matches,deliveries);
        //  console.log(result);
        //  console.timeEnd();

        /* Eighth Program - 8-highest-no-times-dismissed.js  100ms*/
        console.time();
         let result = bowlerSuperOverDeliveries(deliveries);
         console.log(result);
         console.timeEnd();


         /* Ninth Program - 9-superOver-best-economy-bowler.js  50ms*/
        // console.time();
        //  let result =  bowlerSuperOverDetails(deliveries);
        //  console.log(result);
        //  console.timeEnd();

        /*
        fs.writeFileSync(
          path.join(__dirname,"../public/Top10EconomicalBowlersInYear2015.json"),
          JSON.stringify(result),
          "utf8",
          (err) => {
            if (err) console.log(err.name);
          }
        );*/
      });
  });

