function bowlerEconomyCalculator(deliveries,matches,iplSeason){
  let matchSeasonIdDetails = matches.reduce((acc, match) => {
    let id = match.id;
    let season = match.season;
    if ((!acc.hasOwnProperty(id))){
      acc[parseInt(id)] = season;
      return acc;
    }
  }, {});

  let deliveryData = deliveries.map((delivery) => {
    let id = delivery.match_id;
    delivery["season"] = matchSeasonIdDetails[id];
    return delivery;
  });

  let seasonDeliveryData = deliveryData.filter(delivery => delivery.season === iplSeason);

  let bowlerTotalDeliveries = {};
  let bowlerTotalRunsDetails = seasonDeliveryData.reduce((acc,bowlerData) =>{
    let bowlerName = bowlerData.bowler;
    let runAgainstBowling = bowlerData.total_runs;
    if(acc.hasOwnProperty(bowlerName))
    {
      acc[bowlerName] += Number(runAgainstBowling);
      bowlerTotalDeliveries[bowlerName] += 1;
    }
    else{
      acc[bowlerName] = Number(runAgainstBowling);
      bowlerTotalDeliveries[bowlerName] = 1;
    }
    return acc;
  },{})

  let bowlerNamesKeys = Object.keys(bowlerTotalDeliveries);

  let bowlerEconomy = bowlerNamesKeys.reduce((acc,bowlerName) => {
    let runsAgainstBowling = bowlerTotalRunsDetails[bowlerName];
    let countOfBowlerOvers = (bowlerTotalDeliveries[bowlerName]/6);
    acc[bowlerName] = (runsAgainstBowling/countOfBowlerOvers);
    return acc;
  },{})

  let sortedBowlerEconomy = Object.entries(bowlerEconomy)
  .sort(([, item1], [, item2]) => item1 - item2)
  .reduce((rem, [key, val]) => ({ ...rem, [key]: val }), {});

  let economyBowlers = Object.keys(sortedBowlerEconomy);
  let topEconomyBowlers = economyBowlers.slice(0,10);

  let topTenBestEconomyBowlers = topEconomyBowlers.reduce((acc,bowlerData) => {
    acc[bowlerData] = sortedBowlerEconomy[bowlerData];
    return acc;
  },{});

  return topTenBestEconomyBowlers;
}

module.exports = bowlerEconomyCalculator;
