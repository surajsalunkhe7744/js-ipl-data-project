function bowlerSuperOverDetails(deliveries) {
  let superOvers = deliveries.filter((delivery) => delivery.is_super_over != 0);

  let superOverBalls = {};
  let superOverRuns = superOvers.reduce((acc, runDelivery) => {
    let bowler = runDelivery.bowler;
    let totalRunOnBall = runDelivery.total_runs;
    if (acc.hasOwnProperty(bowler)) {
      acc[bowler] += Number(totalRunOnBall);
      superOverBalls[bowler] += 1;
    } else {
      acc[bowler] = Number(totalRunOnBall);
      superOverBalls[bowler] = 1;
    }
    return acc;
  }, {});
  let bowlerKeys = Object.keys(superOverBalls);
  let result = bowlerKeys.reduce((acc, keyIndex) => {
    acc[keyIndex] = superOverRuns[keyIndex] / (superOverBalls[keyIndex] / 6);
    return acc;
  }, {});
  return result;
}

module.exports = bowlerSuperOverDetails;
