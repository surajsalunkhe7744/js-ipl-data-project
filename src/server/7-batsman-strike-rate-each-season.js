// const csvToJson = require("csvtojson");
// const fs = require("fs");
// const path = require("path");

// csvToJson()
//   .fromFile(path.join(__dirname, "../data/deliveries.csv"))
//   .then((deliveries) => {
//     csvToJson()
//       .fromFile(path.join(__dirname, "../data/matches.csv"))
//       .then((matches) => {
        
//         let result = strikeRateCalculator(matches, deliveries);
        
//         fs.writeFileSync(
//           path.join(__dirname, "../public/batsmanStrikeRateEachSeason.json"),
//           JSON.stringify(result),
//           "utf8",
//           (err) => {
//             if (err) console.log(err.name);
//           }
//         );
//       });
//   });

function strikeRateCalculator(matches, deliveries) {
  let matchSeasonIdDetails = matches.reduce((acc, match) => {
    let id = match.id;
    let season = match.season;
    if (!acc.hasOwnProperty(id)) {
      acc[parseInt(id)] = season;
      return acc;
    }
  }, {});

  let seasonDeliveryData = deliveries.map((delivery) => {
    let id = delivery.match_id;
    delivery["season"] = matchSeasonIdDetails[id];
    return delivery;
  });

  let batsmanPlayedBallsData = {};
  let batsmanNames = [];
  let batsmanRunsData = seasonDeliveryData.reduce((runAcc, runs) => {

    let batsmanSeasonRuns = runs.season + runs.batsman;
    let season = runs.season;
    let batsmanRuns = runs.batsman_runs;

    if (runAcc.hasOwnProperty(season)) {
      if (runAcc[season].hasOwnProperty(batsmanSeasonRuns)) {
        runAcc[season][batsmanSeasonRuns] += Number(
          batsmanRuns
        );
        batsmanPlayedBallsData[season][batsmanSeasonRuns] += 1;
      } else {
        runAcc[season][batsmanSeasonRuns] = Number(
          batsmanRuns
        );
        batsmanPlayedBallsData[season][batsmanSeasonRuns] = 1;
        batsmanNames.push(batsmanSeasonRuns);
      }
    } else {
      runAcc[season] = {
        [batsmanSeasonRuns]: Number(batsmanRuns),
      };
      batsmanPlayedBallsData[season] = { [batsmanSeasonRuns]: 1 };
      batsmanNames.push(batsmanSeasonRuns);
    }

    return runAcc;
  }, {});

  let strikeRate = batsmanNames.reduce((acc, batsman) => {
    let season = batsman.substr(0, 4);
    let batsmanData = batsman.substr(4);
    if (acc.hasOwnProperty(season)) {
      acc[season][batsmanData] = (
        (batsmanRunsData[season][batsman] /
          batsmanPlayedBallsData[season][batsman]) *
        100
      ).toPrecision(6);
    } else {
      acc[season] = {
        [batsmanData]: (
          (batsmanRunsData[season][batsman] /
            batsmanPlayedBallsData[season][batsman]) *
          100
        ).toPrecision(6),
      };
    }
    return acc;
  }, {});

  return strikeRate;
}

module.exports = strikeRateCalculator;
