function extraRunCalculator(deliveries, matches, iplSeason) {
  let matchSeasonIdDetails = matches.reduce((acc, match) => {
    let id = match.id;
    let season = match.season;
    if (!acc.hasOwnProperty(id)) {
      acc[parseInt(id)] = season;
      return acc;
    }
  }, {});

  let deliveryData = deliveries.map((delivery) => {
    let id = delivery.match_id;
    delivery["season"] = matchSeasonIdDetails[id];
    return delivery;
  });

  let seasonDeliveryData = deliveryData.filter(
    (delivery) => delivery.season === iplSeason
  );

  let extraRunPerTeam = seasonDeliveryData.reduce((acc, delivery) => {
    let bowlingTeam = delivery.bowling_team;
    let extraRuns = delivery.extra_runs;
    if (acc.hasOwnProperty(bowlingTeam)) {
      acc[bowlingTeam] += Number(extraRuns);
    } else {
      acc[bowlingTeam] = Number(extraRuns);
    }
    return acc;
  }, {});

  return extraRunPerTeam;
}

module.exports = extraRunCalculator;
