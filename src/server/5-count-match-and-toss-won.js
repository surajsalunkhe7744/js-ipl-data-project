function countOfMatchAndTossBothWonTeams(matches) {
  let result = matches.reduce((acc, match) => {
    let matchWinnerTeam = match.winner;
    let tossWinnerTeam = match.toss_winner;
    if (tossWinnerTeam === matchWinnerTeam) {
      if (acc.hasOwnProperty(tossWinnerTeam)) {
        acc[tossWinnerTeam] = acc[tossWinnerTeam] + 1;
      } else {
        acc[tossWinnerTeam] = 1;
      }
    }
    return acc;
  }, {});
  return result;
}

module.exports = countOfMatchAndTossBothWonTeams;
